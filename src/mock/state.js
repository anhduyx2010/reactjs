import { LIST_TASK_KEY } from '../constants'

// const uuidv4 = require('uuid/v4');

let listTasks = localStorage.getItem(LIST_TASK_KEY);

if (!listTasks) {
    listTasks = [];
} else {
    try {
        listTasks = JSON.parse(listTasks)
    } catch (error) {
        listTasks = [];
        localStorage.setItem(LIST_TASK_KEY, '[]');
    }
}



export default listTasks

// [
//     {
//         id: uuidv4(),
//         name: 'ABC Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis ea c',
//         level: 2
//     },

//     {
//         id: uuidv4(),
//         name: 'DEF Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis ea c',
//         level: 0
//     },

//     {
//         id: uuidv4(),
//         name: 'ZBB Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis ea c',
//         level: 1
//     },

//     {
//         id: uuidv4(),
//         name: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis ea c',
//         level: 1
//     },
// ]