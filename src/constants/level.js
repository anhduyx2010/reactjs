export default [
    {
        name: 'Small',
        class: 'info'
    },

    {
        name: 'Medium',
        class: 'warning',
    },

    {
        name: 'High',
        class: 'danger'
    }
]