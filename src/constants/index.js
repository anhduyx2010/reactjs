import TASK_LEVEL from './level'
import SORT from './sort'

const LIST_TASK_KEY = 'listTasks'

export {
    LIST_TASK_KEY,
    TASK_LEVEL,
    SORT
}