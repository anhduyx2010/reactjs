import React, { useState, useMemo, useEffect } from 'react';

import { Container } from 'react-bootstrap'

import initListTask from './mock/state'

import { LIST_TASK_KEY } from './constants'

import './Components/Todo.scss'

import Header from './Components/Header';
import ListTaskTable from './Components/ListTaskTable';
import Control from './Components/Control';

function App() {
  const [listTask, setListTask] = useState(initListTask);
  const [orderBy, setOrderBy] = useState('name');
  const [orderDir, setOrderDir] = useState('DESC')
  const [searchText, setSearchText] = useState('')

  useEffect(() => {
    localStorage.setItem(LIST_TASK_KEY, JSON.stringify(listTask))
  }, [listTask])


  function onSelectSort(orderBy, orderDir) {
    setOrderBy(orderBy)
    setOrderDir(orderDir)
  }

  function onHandleChangSearchText(text) {
    setSearchText(text);
  }

  function handleAddNewwTask({ id, name, level }) {
    listTask.push({
      id,
      name,
      level
    })
    setListTask([...listTask])
  }

  function handleDeleteTask(taskDelete, callback) {
    let taskDeleteFilter = listTask.filter(task => task.id !== taskDelete.id)
    setListTask(taskDeleteFilter)
    callback()
  }

  function handleEditTask(taskEdit, callback) {
    let findIndex = listTask.findIndex(task => task.id === taskEdit.id)
    if (findIndex !== -1) {
      listTask[findIndex].name = taskEdit.name
      listTask[findIndex].level = taskEdit.level

      setListTask([...listTask]);
    }
    callback && typeof callback === 'function' && callback();
  }

  const searchHandle = useMemo(() => {
    const searchHandleFilter = listTask.filter(value => value.name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1)
    return searchHandleFilter
  }, [searchText, listTask])

  const sortHandle = useMemo(() => {
    let returnIndex = 1;
    if (orderDir === 'asc') returnIndex = -1;

    searchHandle.sort((a, b) => {
      if (a[orderBy] < b[orderBy]) return returnIndex
      else if (a[orderBy] > b[orderBy]) return (-1) * returnIndex
      return 0
    })

    return searchHandle
  }, [orderBy, orderDir, searchHandle])

  const injectedPropsControl = {
    orderBy,
    listTask,
    orderDir,
    searchText,
    onSelectSort,
    handleAddNewwTask,
    onHandleChangSearchText,
  }
  return (
    <Container>
      <Header />
      <Control {...injectedPropsControl} />
      <ListTaskTable handleEditTask={handleEditTask} handleDeleteTask={handleDeleteTask} listTask={sortHandle} />
    </Container>
  );
}

export default App;
