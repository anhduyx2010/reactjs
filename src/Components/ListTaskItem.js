import React, { useState, useCallback } from 'react';

import { TASK_LEVEL } from '../constants'
import { Badge, Button } from 'react-bootstrap';

function ListTaskItem({ index, task, handleSetTaskDelete, handleEditTask }) {
    const { name, level } = task;

    const [taskEdit, setTaskEdit] = useState(null)

    const _onHandleClickDelete = useCallback(() => {
        handleSetTaskDelete(task)
    }, [handleSetTaskDelete, task])

    const _onHandleClickEdit = useCallback(() => {
        console.log(task)
        setTaskEdit(task)
    }, [setTaskEdit, task])

    function _onHandleChange(keyField) {
        return (e) => {
            setTaskEdit({
                ...taskEdit,
                [keyField]: e.target.value
            })
        }
    }

    const _onHandleSaveEdit = useCallback(() => {
        setTaskEdit(null)
        typeof handleEditTask === 'function' && handleEditTask(taskEdit, () => {
            setTaskEdit(null)
        })
    }, [handleEditTask, taskEdit])

    return (
        <tr>
            <td className="text-center">{index + 1}</td>
            <td>

                {
                    !taskEdit ? name
                        :

                        <input
                            value={taskEdit.name}
                            onChange={_onHandleChange('name')}
                            type="text"
                            className="form-control"
                            placeholder="Task Name" />
                }

            </td>

            <td className="text-center">

                {
                    !taskEdit ?
                        <Badge className={`badge badge-${TASK_LEVEL[level].class}`}>
                            {TASK_LEVEL[level].name}
                        </Badge>
                        :
                        <select className="form-control"
                            onChange={_onHandleChange('level')}
                            value={taskEdit.level}>
                            {
                                TASK_LEVEL.map((task, index) =>
                                    <option
                                        key={task.name + index}
                                        value={index}>{task.name}
                                    </option>
                                )
                            }
                        </select>
                }


            </td>

            <td>
                {
                    !taskEdit ?
                        <>
                            <Button variant='warning' onClick={_onHandleClickEdit} style={{ marginRight: 10 }}>Edit</Button>
                            <Button variant="danger" onClick={_onHandleClickDelete}>Delete</Button>
                        </>
                        :
                        <>
                            <Button variant='warning' onClick={_onHandleSaveEdit} style={{ marginRight: 4 }}>Save</Button>
                            <Button variant="danger" onClick={() => { setTaskEdit(null) }}>Cancel</Button>
                        </>
                }
            </td>
        </tr>
    );
}

export default ListTaskItem;