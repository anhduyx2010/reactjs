import React from 'react';

import { SORT } from '../constants'


import { Dropdown, Col } from 'react-bootstrap';
import DropdownMenu from 'react-bootstrap/DropdownMenu';
import DropdownItem from 'react-bootstrap/DropdownItem';
import DropdownToggle from 'react-bootstrap/DropdownToggle';

function Sort({ orderBy, orderDir, onSelectDropDown }) {
    return (
        <div>
            <Col xs={12}>
                <div className="form-group">
                    <Dropdown onSelect={onSelectDropDown}>
                        <DropdownToggle>
                            Sort by
                                    </DropdownToggle>
                        <DropdownMenu>
                            {
                                SORT.map((value, index) => {
                                    return <DropdownItem key={index} active={`${orderBy}-${orderDir}` === value.key} eventKey={value.key}>{value.text}</DropdownItem>
                                })
                            }
                        </DropdownMenu>
                        <span className="badge badge-success badge-medium" style={{ textTransform: "uppercase" }}>{`${orderBy} - ${orderDir}`}</span>
                    </Dropdown>
                </div>
            </Col>
        </div>
    );
}

export default Sort;