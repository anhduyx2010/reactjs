import React, { useState, useEffect } from 'react';

import Modal from './Modal';

import { Col, Row, Button } from 'react-bootstrap';
import ListTaskItem from './ListTaskItem';

function ListTaskTable({ listTask: listTasks, handleEditTask, handleDeleteTask }) {

    const [isOpenModal, setIsOpenModal] = useState(false)
    const [taskDelete, setTaskDelete] = useState(null)

    useEffect(() => {
        if (taskDelete) {
            setIsOpenModal(true)
        }
        else {
            setIsOpenModal(false)
        }
    }, [taskDelete])

    function handleSetTaskDelete(task) {
        setTaskDelete(task)
    }

    function _onHandleSubmit() {
        handleDeleteTask(taskDelete, () => {
            setIsOpenModal(false)
            setTaskDelete(null)
        })
    }

    return (
        <Row>
            {/* FORM : END */}
            <Col xs={12} style={{ marginTop: 10 }}>
                <div className="panel panel-success">
                    <table className="table table-hover ">
                        <thead>
                            <tr>
                                <th style={{ width: '10%' }} className="text-center">#</th>
                                <th>Task</th>
                                <th style={{ width: '20%' }} className="text-center">Level</th>
                                <th style={{ width: '160px' }}>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                listTasks && listTasks.length > 0 && listTasks.map((task, index) => {
                                    return <ListTaskItem handleEditTask={handleEditTask} handleSetTaskDelete={handleSetTaskDelete} key={task.id} task={task} index={index} />
                                })
                            }
                        </tbody>
                    </table>

                    <Modal
                        title="Warning!!!"
                        isVisible={isOpenModal}
                        renderFooter={() => {
                            return (
                                <>
                                    <Button
                                        style={{ marginRight: 10 }}
                                        onClick={_onHandleSubmit}
                                    >
                                        Confirm
                                    </Button>

                                    <Button
                                        onClick={() => setIsOpenModal(false)}
                                        variant="secondary"
                                    >
                                        Cancel
                                    </Button>
                                </>
                            )
                        }}
                    >
                        {
                            taskDelete && <strong>Bạn có chắc chắn muốn xóa task : "<small>{taskDelete.name}</small>" </strong>
                        }
                    </Modal>
                </div>
            </Col>
        </Row >
    );
}

export default ListTaskTable;
