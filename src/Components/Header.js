import React from 'react';
import { Row, Col } from 'react-bootstrap';

function Header(props) {

    return (
        <Row>
            <Col xs={12}>
                <div className="page-header">
                    <h1>Project 01 - ToDo List <small>ReactJs</small></h1>
                </div>
            </Col>
        </Row>
    );
}

export default Header;