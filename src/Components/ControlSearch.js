import React, { useState, useEffect } from 'react';
import { Button } from 'react-bootstrap';

function ControlSearch({ searchText, onHandleChangSearchText }) {
    const [text, setText] = useState('')

    useEffect(() => {
        setText(searchText)
    }, [searchText])

    function _onHandleChangSearchText(e) {
        return onHandleChangSearchText(e.target.value)
    }

    function _onHandleChangClearSearchText(e) {
        return onHandleChangSearchText('')
    }

    return (
        <div>
            <div>
                <div className="input-group">
                    <input
                        value={text}
                        onChange={_onHandleChangSearchText}
                        type="text"
                        className="form-control"
                        placeholder="Search for..." />
                    <span className="input-group-append">
                        <Button onClick={_onHandleChangClearSearchText} variant="info">Clear!</Button>
                    </span>
                </div>
            </div>
        </div>
    );
}

export default ControlSearch;