import React, { useEffect, useCallback } from 'react';
import './Modal.scss'

function Modal({ isVisible, children, isRenderHeader, title, renderFooter, onCancle }) {
    const _onCancel = useCallback(() => {
        if (onCancle && typeof onCancle === 'function') {
            onCancle()
        }
    }, [onCancle])

    function _renderFooter() {
        if (renderFooter && typeof renderFooter === 'function') {
            return renderFooter()
        }

        return (
            <>
                <button onClick={_onCancel}>Cancel</button>
                <button>Ok</button>
            </>
        )
    }

    useEffect(() => {
        document.addEventListener('keyup', (event) => {
            if (event.keyCode === 27) {
                return _onCancel()
            }
        })
    }, [_onCancel])

    useEffect(() => {
        if (isVisible) {
            document.querySelector('body').classList.add('modal-open')
        }
        else {
            document.querySelector('body').classList.remove('modal-open')
        }
    }, [isVisible])
    return (
        <div className={`modal-wrapper ${isVisible ? '_show' : ''}`}>
            <div className="mark" onClick={_onCancel}></div>
            <div className="modal-content">
                <div className="modal-header">
                    {
                        isRenderHeader && <div><h4>{title}</h4></div>
                    }

                    <div onClick={_onCancel} className="close-modal">
                        <ion-icon name="close-outline"></ion-icon>
                    </div>
                </div>

                <div className="modal-body">
                    {children}
                </div>

                <div className="modal-footer">
                    {
                        _renderFooter()
                    }
                </div>
            </div>
        </div>
    );
}

export default Modal;