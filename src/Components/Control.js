import React from 'react';

import Sort from './ControlSort';
import ControlSearch from './ControlSearch';


import { Row, Col } from 'react-bootstrap';
import ControlAddNew from './ControlAddNew';

function Control({ orderBy, orderDir, onSelectSort, searchText, onHandleChangSearchText, handleAddNewwTask }: props.injectedPropsControl) {
    function onSelectDropDown(eventKey) {
        let [orderBy, orderDir] = eventKey.split('-')
        onSelectSort && typeof onSelectSort === 'function' && onSelectSort(orderBy, orderDir)
    }

    return (
        <Row>
            <Col xs={12} lg={6}>
                {/* CONTROL (SEARCH + SORT + ADD) : START */}
                <div>
                    <Row>
                        {/* SORT : START */}
                        <Sort
                            onSelectDropDown={onSelectDropDown}
                            orderBy={orderBy}
                            orderDir={orderDir}
                        />
                        {/* SORT : END */}
                    </Row>

                    {/* SEARCH : START */}
                    <ControlSearch searchText={searchText} onHandleChangSearchText={onHandleChangSearchText} />
                    {/* SEARCH : END */}
                </div>
                {/* CONTROL (SEARCH + SORT + ADD) : END */}
            </Col>


            {/* FORM : START */}
            <Col xs={12} lg={6}>
                {/* ADD : START */}
                <ControlAddNew handleAddNewwTask={handleAddNewwTask} />
                {/* ADD : END */}
            </Col>
        </Row >
    );
}

export default Control;