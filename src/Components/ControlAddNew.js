import React, { useState } from 'react';

import uuidv4 from 'uuid/v4'

import Modal from './Modal'

import { TASK_LEVEL } from '../constants'


import { Button } from 'react-bootstrap';

function ControlAddNew({ handleAddNewwTask }) {

    const initTask = {
        name: '',
        level: 0
    }

    const [isOpenModal, setIsOpenModal] = useState(false)
    const [task, setTask] = useState({
        name: '',
        level: 0
    })

    function _onHandleChange(keyField) {
        return (e) => {
            setTask({
                ...task,
                [keyField]: e.target.value
            })
        }
    }

    function _onHandleSubmit() {
        let data = {
            id: uuidv4(),
            ...task
        }
        handleAddNewwTask(data);
        setIsOpenModal(false);
        setTask(initTask)
    }

    return (
        <div>
            <div className="form-group add-task">
                <button onClick={() => setIsOpenModal(true)} variant="info" className="btn-block">Add Task</button>
            </div>
            {
                isOpenModal && <Modal
                    isRenderHeader={true}
                    title="Add New Task"
                    isVisible={true}
                    renderFooter={() => {
                        return (
                            <>
                                <Button variant="primary" onClick={_onHandleSubmit}>Submit</Button>
                                <Button onClick={() => setIsOpenModal(false)} variant="secondary">Cancel</Button>
                            </>
                        )
                    }}

                >
                    <label className="sr-only">label</label>
                    <input
                        value={task.name}
                        onChange={_onHandleChange('name')}
                        type="text"
                        className="form-control"
                        placeholder="Task Name" />
                    <div className="form-group">
                        <label className="sr-only">label</label>
                        <select className="form-control"
                            onChange={_onHandleChange('level')}>
                            {
                                TASK_LEVEL.map((task, index) =>
                                    <option
                                        key={task.name + index}
                                        value={index}>{task.name}
                                    </option>)
                            }
                        </select>
                    </div>
                </Modal>
            }
        </div>
    );
}

export default ControlAddNew;